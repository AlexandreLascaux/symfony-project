<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200111110923 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE project (id INT AUTO_INCREMENT NOT NULL, subjects_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, max_student INT NOT NULL, mark INT NOT NULL, INDEX IDX_2FB3D0EE94AF957A (subjects_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE speaker (id INT AUTO_INCREMENT NOT NULL, firstname VARCHAR(255) NOT NULL, lastname VARCHAR(255) NOT NULL, age INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE speaker_subject (speaker_id INT NOT NULL, subject_id INT NOT NULL, INDEX IDX_FA7C7C28D04A0F27 (speaker_id), INDEX IDX_FA7C7C2823EDC87 (subject_id), PRIMARY KEY(speaker_id, subject_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE student (id INT AUTO_INCREMENT NOT NULL, firstname VARCHAR(255) DEFAULT NULL, lastname VARCHAR(255) DEFAULT NULL, age INT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE subject (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE project ADD CONSTRAINT FK_2FB3D0EE94AF957A FOREIGN KEY (subjects_id) REFERENCES subject (id)');
        $this->addSql('ALTER TABLE speaker_subject ADD CONSTRAINT FK_FA7C7C28D04A0F27 FOREIGN KEY (speaker_id) REFERENCES speaker (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE speaker_subject ADD CONSTRAINT FK_FA7C7C2823EDC87 FOREIGN KEY (subject_id) REFERENCES subject (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE speaker_subject DROP FOREIGN KEY FK_FA7C7C28D04A0F27');
        $this->addSql('ALTER TABLE project DROP FOREIGN KEY FK_2FB3D0EE94AF957A');
        $this->addSql('ALTER TABLE speaker_subject DROP FOREIGN KEY FK_FA7C7C2823EDC87');
        $this->addSql('DROP TABLE project');
        $this->addSql('DROP TABLE speaker');
        $this->addSql('DROP TABLE speaker_subject');
        $this->addSql('DROP TABLE student');
        $this->addSql('DROP TABLE subject');
    }
}
