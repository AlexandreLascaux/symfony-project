<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200113165620 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE student_project (student_id INT NOT NULL, project_id INT NOT NULL, INDEX IDX_C2856516CB944F1A (student_id), INDEX IDX_C2856516166D1F9C (project_id), PRIMARY KEY(student_id, project_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE student_project ADD CONSTRAINT FK_C2856516CB944F1A FOREIGN KEY (student_id) REFERENCES student (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE student_project ADD CONSTRAINT FK_C2856516166D1F9C FOREIGN KEY (project_id) REFERENCES project (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE speaker_subject');
        $this->addSql('ALTER TABLE project DROP FOREIGN KEY FK_2FB3D0EE23EDC87');
        $this->addSql('DROP INDEX IDX_2FB3D0EE23EDC87 ON project');
        $this->addSql('ALTER TABLE project CHANGE subject_id subjects_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE project ADD CONSTRAINT FK_2FB3D0EE94AF957A FOREIGN KEY (subjects_id) REFERENCES subject (id)');
        $this->addSql('CREATE INDEX IDX_2FB3D0EE94AF957A ON project (subjects_id)');
        $this->addSql('ALTER TABLE subject ADD speakers_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE subject ADD CONSTRAINT FK_FBCE3E7A42AB4241 FOREIGN KEY (speakers_id) REFERENCES speaker (id)');
        $this->addSql('CREATE INDEX IDX_FBCE3E7A42AB4241 ON subject (speakers_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE speaker_subject (speaker_id INT NOT NULL, subject_id INT NOT NULL, INDEX IDX_FA7C7C2823EDC87 (subject_id), INDEX IDX_FA7C7C28D04A0F27 (speaker_id), PRIMARY KEY(speaker_id, subject_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE speaker_subject ADD CONSTRAINT FK_FA7C7C28D04A0F27 FOREIGN KEY (speaker_id) REFERENCES speaker (id) ON UPDATE NO ACTION ON DELETE CASCADE');
        $this->addSql('ALTER TABLE speaker_subject ADD CONSTRAINT FK_FA7C7C2823EDC87 FOREIGN KEY (subject_id) REFERENCES subject (id) ON UPDATE NO ACTION ON DELETE CASCADE');
        $this->addSql('DROP TABLE student_project');
        $this->addSql('ALTER TABLE project DROP FOREIGN KEY FK_2FB3D0EE94AF957A');
        $this->addSql('DROP INDEX IDX_2FB3D0EE94AF957A ON project');
        $this->addSql('ALTER TABLE project CHANGE subjects_id subject_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE project ADD CONSTRAINT FK_2FB3D0EE23EDC87 FOREIGN KEY (subject_id) REFERENCES subject (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX IDX_2FB3D0EE23EDC87 ON project (subject_id)');
        $this->addSql('ALTER TABLE subject DROP FOREIGN KEY FK_FBCE3E7A42AB4241');
        $this->addSql('DROP INDEX IDX_FBCE3E7A42AB4241 ON subject');
        $this->addSql('ALTER TABLE subject DROP speakers_id');
    }
}
