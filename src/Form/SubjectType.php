<?php

namespace App\Form;

use App\Entity\Subject;
use App\Entity\Speaker;
use App\Repository\SpeakerRepository;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SubjectType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('speaker', EntityType::class, [
                'class' => Speaker::class,
                'query_builder' => function(EntityRepository $er){
                    return $er->createQueryBuilder('s')
                        ->groupBy('s')
                        ->having("COUNT(subjects) < 2")
                        ->leftJoin('s.subjects', 'subjects');
                },
                'choice_label' => function ($speaker) {
                    return $speaker->getFirstname() . ' ' . $speaker->getLastname();
                },
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Subject::class,
        ]);
    }
}
