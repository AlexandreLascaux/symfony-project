<?php

namespace App\Form;

use App\Entity\Project;
use App\Entity\Student;
use App\Entity\Subject;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProjectType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('maxStudent')
            ->add('mark')
            ->add('subject', EntityType::class, [
                'class' => Subject::class,
                'choice_label' => 'name',
            ])
            ->add('students', EntityType::class, [
                'class' => Student::class,
                'query_builder' => function(EntityRepository $er){
                    return $er->createQueryBuilder('s')
                        ->leftJoin('s.projects', 'projects')
                        ->groupBy('s.id')
                        ->having('COUNT(projects.id) < 4')
                        ;
                },
                'choice_label' => 'firstname',
                'multiple' => true,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Project::class,
        ]);
    }
}
