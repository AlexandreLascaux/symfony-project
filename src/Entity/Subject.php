<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SubjectRepository")
 */
class Subject
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Speaker", inversedBy="subjects")
     * @ORM\JoinColumn(nullable=true)
     */
    private $speaker;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Project", mappedBy="subject")
     */
    private $projects;

    public function __construct()
    {
        $this->projects = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSpeaker(): ?Speaker
    {
        return $this->speaker;
    }

    public function setSpeaker(?Speaker $speaker): self
    {
        $this->speaker = $speaker;

        return $this;
    }

    /**
     * @return Collection|Project[]
     */
    public function getProjects(): Collection
    {
        return $this->projects;
    }

    public function addProject(Project $project): self
    {
        if (!$this->projects->contains($project)) {
            $this->projects[] = $project;
            $project->setSubject($this);
        }

        return $this;
    }

    public function removeProject(Project $project): self
    {
        if ($this->projects->contains($project)) {
            $this->projects->removeElement($project);
            // set the owning side to null (unless already changed)
            if ($project->getSubject() === $this) {
                $project->setSubject(null);
            }
        }

        return $this;
    }

    public function averageMark(): ?int {
        $notes = 0;
        $average = 0;
        $projectsNumber = $this->projects->count();
        foreach ($this->projects as $value){
            if(is_int($value->getMark())) {
                $notes += $value->getMark();
            } else {
                $projectsNumber -= 1;
            }
        }
        if($projectsNumber != 0) {
            $average = $notes / $projectsNumber;
        } else {
            $average = 0;
        }
        return $average;
    }
}
