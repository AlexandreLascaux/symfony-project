# Installation

Pour installer le projet, il faut tout d'abord cloner le docker-symfony.

```
git clone https://gitlab.com/RealSmir/docker-symfony-project.git docker-symfony
cd docker-symfony
```

Une fois cloné, faites la commande suivante :

```
git clone https://gitlab.com/RealSmir/symfony-project.git symfony
```

Après avoir cloné les deux repo, faites les commandes suivante :

```
docker-compose up -d
cd symfony
yarn add webpack-notifier@^1.6.0 --dev
yarn add sass-loader@^7.0.1 node-sass --dev
yarn encore dev --watch
```

Ouvrez une nouvelle fenêtre de terminal et placez-vous dans docker-symfony puis faites les commandes suivantes :

```
docker exec -it php-fpm sh
composer install
php bin/console d:s:u --dump-sql
php bin/console d:s:u --force
``` 

Le site est désormais accessible à [cette adresse](http://symfony.localhost)